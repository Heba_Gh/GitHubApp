//
//  APIEndpoints.swift
//  GitHubApp
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import Foundation

struct APIEndpoints
{
    
    static func getUser(with name: String) -> Endpoint {

        return Endpoint(path: "users/",
                        method: .get , queryParametersEncodable : name)
    }

}



