

import Foundation
import RxSwift
protocol GetUserProfileUseCase
{
    func execute(with name: String) -> Observable<Result<User, AppError>>
}

final class DefaultGetUserProfileUseCase : GetUserProfileUseCase {
    
    private let defaultRepository: Repository
    
    
    init(defaultRepository: Repository)
    {
        self.defaultRepository = defaultRepository
    }
    
    func execute(with name: String) -> Observable<Result<User, AppError>> {
        
        return Observable<Result<User, AppError>>.create { observer in
            if !isOnline
            {
                observer.onNext(Result.failure(AppError(reason: .networkError, message: "Please check your internet connection")))
            }
            else
            {
            observer.onNext(Result.loading)
            self.defaultRepository.fetchUser(with: name, completion: { result in
                
                observer.onNext(result)
            })
            }
            return Disposables.create()
        }
    }
}

