

import Foundation
protocol Repository
{

    func fetchUser(with name : String , completion: @escaping (Result<User, AppError>) -> Void)
}

