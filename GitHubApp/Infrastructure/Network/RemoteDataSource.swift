
import Foundation
import Alamofire

// MARK: - GetFailureReason
public struct AppError : Error
{
    var reason : GetFailureReason?
    var message : String?
    init(reason :GetFailureReason , message : String? )
    {
        self.reason = reason
        self.message = message
    }
}
enum GetFailureReason: Int, Error
{
    case unAuthorized = 401
    case userNameOrPasswordUnCorrect = 402
    case Forbidden = 403
    case notFound = 404
    case unprocessableEntity = 422
    case notModified = 304
    case networkError = 700
}

// MARK: - Remote Data Source
class RemoteDataSource {
    
 func sendRequest<T>(_ request: URLRequest , param : [String : Any] ,completion: @escaping  (Result<T, AppError>) -> Void) where T: Decodable {
        
        AF.request(request.url!.absoluteString, method: request.method!, parameters: param , headers: request.headers).responseDecodable(of: T.self)
        {
            (response) in
            
            switch response.result {
                
            case .success(let res):
                
                    print(res)
                completion(.success(data: res , code : response.response?.statusCode ?? 200 , message: ""))
            
            case .failure(_):
                if let statusCode = response.response?.statusCode,
                   let reason = GetFailureReason(rawValue: statusCode) {
                    print(reason)
                    let appError = AppError(reason: reason, message: "unknown error")
                    completion(.failure(appError))
                   
                }else{
                    completion(.failure(nil))
                }
            }
        }
    }
    
}


enum Result<T, U: Error> {
    case success(data: T , code : Int , message : String)
    case failure(U?)
    case loading
}

enum EmptyResult<U: Error> {
    case success
    case failure(U?)
    case loading
}


