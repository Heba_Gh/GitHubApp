

import Foundation

public struct ApiDataNetworkConfig {
    public let baseURL: URL
    public let headers: [String: String]
 
     public init() {
        self.baseURL =  URL(string: "https://api.github.com/")!
        self.headers = generateHeaders()
     
    }
}
func generateHeaders() ->[String:String]
{
    var headers = [String:String]()

    let INPUT_VERSION = "version"
    let INPUT_DEVICE = "app"
    let DEVICE_UUID = "device_uuid"
    let INPUT_DIVICE_TYPE = "DEVICE_TYPE"
    let INTPUT_ACCEPT = "Accept"

    headers[INPUT_VERSION] = VERSION
    headers[INPUT_DEVICE] = DEVICE
    headers[DEVICE_UUID] = DEVICE_UUID
    headers[INPUT_DIVICE_TYPE] = "ios"
 
    headers[INTPUT_ACCEPT] = "application/json"
   
  
    return headers
}

public protocol NetworkCancellable {
    func cancel()
}
