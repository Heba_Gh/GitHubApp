//
//  BaseViewController.swift
//  GitHubApp
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import UIKit
let errorViewTag = 2020
class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func showFullScreenLoading()
    {
        
        LoadingOverlay.shared.showOverlay(view: self.view)
        
    }
    
    func finishFullScreenLoading()
    {
        // DispatchQueue.main.async {
        
        LoadingOverlay.shared.hideOverlayView()
        // }
        
        
    }
    
    public func showFooterError(delegate: PaginationErrorViewDelegate, tableView: UITableView) {
        
        let allviews = Bundle.main.loadNibNamed("PaginationErrorView", owner: self, options: nil)
        
        if let footerView = allviews?.first as? PaginationErrorView {
            
            footerView.delegate = delegate
            
            tableView.tableFooterView = footerView
            
        }
    }
    
    public func showFullScreenError(view: UIView, message: String = "" , title : String = "" , delegate : CustomErrorViewDelegate) {
        
        let allviews = Bundle.main.loadNibNamed("CustomErrorView", owner: self, options: nil)
        
        if let errorView = allviews?.first as? CustomErrorView {

            errorView.delegate = delegate
         
            errorView.tag = errorViewTag
          
          
            errorView.center =  CGPoint(x: screenWidth  / 2,
                                         y: (screenHeight)  / 2)
            
            view.addSubview(errorView)
            
        }
    }
    
    public func hideFullScreenError() {
        
        if let errorView = self.view.viewWithTag(errorViewTag) {
            errorView.removeFromSuperview()
        }
    }
    
}
