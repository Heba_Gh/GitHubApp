//
//  SearchOnUserViewModel.swift
//  GitHubApp
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import Foundation
import RxSwift

final class SearchOnUserViewModel
{

    private let getUserProfileUseCase: GetUserProfileUseCase
  
    // MARK: - Init

    init(getUserProfileUseCase: GetUserProfileUseCase)
    {
        self.getUserProfileUseCase = getUserProfileUseCase
    }

    // MARK: - SEARCH ON USER BY NAME
   
    func search(with name : String) -> Observable<Result<User, AppError>>
    {
        return getUserProfileUseCase.execute(with: name)
    }

}
