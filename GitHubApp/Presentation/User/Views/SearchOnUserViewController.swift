//
//  SearchOnUserViewController.swift
//  GitHubApp
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import UIKit
import RxSwift
class SearchOnUserViewController: UIViewController {
 
    var viewModel : SearchOnUserViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    @IBAction func search(_ sender: Any) {
        
        let observable = viewModel.search(with: "h")
         observable.subscribe(onNext: {
              result in
              switch result
              {
              case .loading:
                  self.showFullScreenLoading()
                  break
              case .failure(let error):
                  self.finishFullScreenLoading()
                  messageView.showWarningMessage(message: error?.message ?? "unknown error")
                  break
              case .success(_, _ , _):
                  self.finishFullScreenLoading()
                  let vc = ViewController.instantiate(storyboard: .Main)
                  self.present(vc, animated: true, completion: nil)
                  break
              }
          }).disposed(by: disposeBag)
    }
    

}
